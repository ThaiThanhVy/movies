import React from 'react'
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { localServ } from '../../Services/LocalService';
export default function UserNav() {

    // Lấy về userInfor bằng useSelector = mapStateToProps
    let user = useSelector((state) => {
        return state.userReducer.userInfor
    })

    console.log('user: ', user);

    let handleRemove = () => {
        localServ.user.remove()
        setTimeout(() => {
            window.location.href = "/"
        }, 1000)
    }

    let renderContent = () => {
        if (user) {
            return <>
                <span className='mr-3 underline font-medium'>{user.hoTen}</span>
                <button onClick={() => { handleRemove() }} className='self-center px-8 py-3 font-semibold rounded dark:bg-violet-400 dark:text-gray-900'>
                    Đăng Xuất
                </button>
                {/* border  border-none hover:scale-110 hover:duration-500  rounded px-5 py-2 text-light bg-green-500 */}
            </>
        } else {
            return <>
                <NavLink to="/login">
                    <div className="items-center flex-shrink-0 hidden lg:flex">
                        <button className="self-center px-8 py-3 rounded">Đăng Nhập</button>
                        <button className="border self-center px-8 py-3 font-semibold rounded bg-sky-400 text-gray-900">Đăng Kí</button>
                    </div>

                </NavLink>
            </>
        }
    }
    // border  border - none hover: scale - 110 hover: duration - 500  rounded px - 5 py - 2 text - light bg - green - 500
    return (
        <div className='space-x-5'>

            {renderContent()}


        </div >
    )
}
