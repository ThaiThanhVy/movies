import { combineReducers } from "redux";
import userReducer from './ReducerUser'
import reducerCarousel from "./ReducerCarousel";
import reducerManageTheatre from "./ReducerManageThetre";
import reducerManageMovie from "./ReducerManageMovie";

const rootReducer = combineReducers({
    userReducer,
    reducerCarousel,
    reducerManageTheatre,
    reducerManageMovie,
})
export default rootReducer