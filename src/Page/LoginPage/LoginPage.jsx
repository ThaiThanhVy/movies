import { Button, Checkbox, Form, Input, message } from 'antd';
import { movieServ } from '../../Services/UserService';
import React from 'react';
import { localServ } from '../../Services/LocalService';
import { useNavigate } from 'react-router-dom'
import { SET_USER } from '../../Redux/Constants/ConstantsUser';
import { useDispatch } from 'react-redux';



const LoginPage = () => {

    let navigate = useNavigate()
    let dispatch = useDispatch();
    const onFinish = (values) => {
        movieServ.postLogin(values)
            .then((res) => {
                // Đây là cái object mình cần lưu

                // Lưu ý dùng window để chuyển trang sẽ lamg cho trang web bị load lại
                // Nên dùng 

                // Lưu vào localStorage
                localServ.user.set(res.data.content)

                // dispatch to store

                dispatch({
                    type: SET_USER,
                    payload: res.data.content
                })
                console.log('res.data.content: ', res.data.content);

                // Chuyển hướng trang
                setTimeout(() => {
                    navigate("/")
                }, 1500)

                // Thông báo đăng nhập thành công hoặc thất bại
                message.success('Đăng Nhập Thành Công')
                console.log(res);
            })
            .catch((err) => {
                message.success('Đăng Nhập Thất Bại')
                console.log(err);
            });
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    const styleDangNhap = {
        color: '#000',
        fontSize: '32px',
        fontWeight: ' 700',
        marginBottom: '28px',
    }

    const background = {
        backgroundImage: 'url(./img/anhPhim.jpg)',
        height: '1111px',
        marginTop: 0,
        position: 'relative',
        overflow: 'hidden',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
    }

    return (

            <div style={background}>
                <div className='container mx-auto w-50 h-70 ' style={{ backgroundColor: 'rgba(150, 146, 148, 0.75)', marginTop: 129 }}>
                    <br />
                    <h3 className='flex container justify-center' style={styleDangNhap}>Đăng Nhập</h3>
                    <div className='container mx-auto w-70 flex justify-center'>
                        <div className='mt-2 w-80'>
                            <Form
                                name="basic"
                                layout="vertical"
                                labelCol={{
                                    span: 8,
                                }}
                                wrapperCol={{
                                    span: 24,
                                }}
                                initialValues={{
                                    remember: true,
                                }}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                                autoComplete="off"
                            >
                                <Form.Item
                                    label="Username"
                                    name="taiKhoan"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your username!',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label="Password"
                                    name="matKhau"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your password!',
                                        },
                                    ]}
                                >
                                    <Input.Password />
                                </Form.Item>

                                <Form.Item
                                    name="remember"
                                    valuePropName="checked"
                                    wrapperCol={{
                                        offset: 8,
                                        span: 16,
                                    }}
                                >
                                </Form.Item>
                                <Form.Item
                                    wrapperCol={{
                                        offset: 8,
                                        span: 16,
                                    }}
                                >
                                    <Button type="primary" htmlType="submit">
                                        Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div >
                </div>
            </div>
    );
};

export default LoginPage;