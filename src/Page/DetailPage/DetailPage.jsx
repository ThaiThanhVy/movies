import { Tabs } from 'antd'
import axios from 'axios'
import React from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import '../../assets/styles/circleRatting.scss'
import { movieServ } from '../../Services/CarouselService'



export default function DetailPage(props) {


    let img = {
        height: '314px',
        display: ' block',
        position: 'relative',
        borderRadius: '4px',
        backgroundSize: 'cover',
        backgroundRepeat: ' no-repeat',
        backgroundPosition: 'center',
    }

    let ratting = {
        marginLeft: 200,
        marginTop: 20,
        fontSize: 50,
    }

    let { detailMovie } = useSelector(state => state.reducerManageMovie)

    console.log('detailMovie: ', detailMovie);

    // let dispatch = useDispatch([])


    // useEffect(() => {
    //     let { id } = props.match.params

    // }, [])

    // useEffect(() => {
    //     let { id } = props.match.params
    //     movieServ.getMoviesManage()
    //         .then((res) => {
    //             console.log('carousel', res)
    //             dispatch({
    //                 type: 'DETAIL_MOVIE',
    //                 detailMovie: res.data.content,
    //             })
    //         })
    //         .catch((err) => {
    //             console.log(err)
    //         })
    // }, [])

    return (
        <div className='bg-gray-800 pb-5' style={{ minHeight: '100vh' }} >
            <div className='grid grid-cols-12 pt-5 mb-5' style={{ marginRight: 150 }}>
                <div className='grid grid-cols-2 cols-span-4 col-start-4 pt-40' style={{ width: 850 }}>
                    <div className='grid grid-cols-2'>
                        <img style={img} src="https://movienew.cybersoft.edu.vn/hinhanh/ban-tay-diet-quy.png" alt="" />
                        <div className='ml-4 mt-5'>
                            <p className='text-white'>Ngay Chieu</p>
                            <p className='text-white'>Ten Phim</p>
                            <p className='text-white'>Thoi Luong</p>
                            <button className='btn btn-danger'>Mua Ve</button>
                        </div>
                    </div>
                    <div className='grid grid-cols-4' style={ratting}>
                        <div className="pacss-wrapper">
                            <span className="pacss-foreground">
                                <span className="pacss-number">10</span>
                            </span>
                            <span className="pacss pacss-100 pacss-big" />
                        </div>
                    </div>
                </div>
            </div>
            <div className='bg-white flex justify-center container'>
                <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
                    <Tabs.TabPane tab="Tab 1" key="1">
                        Content of Tab Pane 1
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Tab 2" key="2">
                        Content of Tab Pane 2
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Tab 3" key="3">
                        Content of Tab Pane 3
                    </Tabs.TabPane>
                </Tabs>
            </div>
        </div >
    )
}
