import React, { useEffect, useState } from 'react'
import HomeCarousel from '../../components/HeaderTheme/HomeCarousel'
import { movieServ } from '../../Services/MovieService'
import ItemMovies from './ItemMovies'
import TabsMovies from './TabsMovies'


export default function HomePage() {

    // tạo state và setState bằng useState
    const [movies, setMovies] = useState([])

    useEffect(() => {
        movieServ.getListMovie()
            .then((res) => {
                console.log(res)
                setMovies(res.data.content);
            })
            .catch((err) => {
                console.log(err)
            })
    }, [])

    const renderMovies = () => {
        return movies.map((data, index) => {
            return <ItemMovies key={index} data={data} />
        })
    }


    return (

        <div>
            <HomeCarousel />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div className='grid grid-cols-5 gap-10 container mb-5'>
                {renderMovies()}

                {/* Ghi chữ hello ở đây thì nó sẽ chạy không được */}
                {/* Nằm chữ Hello nó là childer */}
                {/* Nên muốn nó chạy thì phải thêm childer vào cái file muốn lấy và cho cái props data = {} */}
                {/* VD */}
                {/* <ItemMovies>Ví Dụ Children</ItemMovies> */}
            </div>
            <div className='container' id='tabsmovies'>
                <TabsMovies />
            </div>
        </div>
    )
}
