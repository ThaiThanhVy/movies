import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css';
import Layout from './Layout/Layout';
// npm i ant design
import "antd/dist/antd.css";
import DetailPage from './Page/DetailPage/DetailPage';
import HomePage from './Page/HomePage/HomePage';
import LoginPage from './Page/LoginPage/LoginPage';
import Contact from './Page/Contact/Contact';


function App() {
  return (
    <div>
      {/* BrowserRouter Giống như Provider có cái này thì Routes mới hoạt động */}
      <BrowserRouter>
        {/* Phân Trang */}
        <Routes>
          <Route
            path="/"
            element={
              <Layout Component={HomePage} />
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route path="/contact" element={<Contact />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
